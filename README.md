##To run the application, execute
`mvn clean install spring-boot:run`

##How it works
The StatsService doesn't store all the transactions that fit the 60 seconds window, because obviously this approach 
would require O(N) memory. Rather it maintains a hash table with 60 'buckets', each of them corresponds to one second.
Whenever new transaction arrives, we calculate the index of the bucket by 'normalizing' the milliseconds timestamp by 
dividing it by 1000 (to get number of seconds) and then doing modulo 60. Once we get the bucket index we check if 
given bucket is empty or not. If it is, then we just put new value there. If it's not, we check whether
the existing value is still within the window. If it is, then we merge the old with the new ones, if not, then we just 
overwrite the old one with the new.
When client requests statistics by calling 'GET' /statistics we traverse our storage, filter elements which are only relevant
for the time window of [t-60, t], and collect the statistics. This solution has O(1) time and memory complexity.
This idea was influenced by this research: 
http://www-cs-students.stanford.edu/~datar/papers/sicomp_streams.pdf

The statistics are stored using java.util.DoubleSummaryStatistics class which is a perfect match for our use case.

Technologies used:
* Java 8,
* Spring boot,
* Sprint test,
* Junit,
* Mockito,
* Lombok

## API description

**Post transaction**
----
Post new transaction providing its amount and timestamp

* **URL**
  /api/transactions
* **Method**
  `POST`
* **Example request body**
  ```
     {
         "amount": 12.3,
          "timestamp": 1478192204000
     }
     ```
     
* **Required request header**
  `Content-Type: application/json`
* **Response - if transaction is inside time window** `201 Created`
* **Response - if transaction is outside time window** `204 No Content`
  
**Get statistics**
----
Get statistics for the transactions which are currently within the window

* **URL**
  /api/statistics
* **Method**
  `GET`
* **Response**
  * **Code:** 200 Ok
  * **Example Body:** `"count":12058,"sum":12058.0,"min":1.0,"max":1.0,"average":1.0}`