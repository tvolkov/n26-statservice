package org.tvolkov.dto;

import lombok.Getter;
import lombok.Setter;

public class Transaction {
    @Getter
    @Setter
    private double amount;

    @Getter
    @Setter
    private long timestamp;
}
