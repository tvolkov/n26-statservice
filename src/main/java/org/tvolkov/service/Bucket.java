package org.tvolkov.service;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.DoubleSummaryStatistics;

@AllArgsConstructor
public class Bucket {
    @Getter
    private long timestamp;

    @Getter
    private DoubleSummaryStatistics doubleSummaryStatistics;
}
