package org.tvolkov.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.tvolkov.dto.Transaction;

import java.util.DoubleSummaryStatistics;
import java.util.concurrent.ConcurrentHashMap;

import static java.util.Objects.requireNonNull;

@Service
public class StatsService {

    @Value("${sliding.window.size}")
    private int slidingWindowSize;
    private ConcurrentHashMap<Integer, Bucket> storage = new ConcurrentHashMap<>(slidingWindowSize);
    private BucketMerger bucketMerger;

    @Autowired
    public StatsService(BucketMerger bucketMerger) {
        this.bucketMerger = bucketMerger;
    }

    public boolean processTransaction(Transaction transaction) {
        requireNonNull(transaction);
        long currentInstant = System.currentTimeMillis();
        if (hasExpired(transaction.getTimestamp(), currentInstant)) return false;
        Bucket newBucket = createNewBucket(transaction);
        int newBucketIndex = getBucketIndex(currentInstant, transaction.getTimestamp());

        storage.merge(newBucketIndex, newBucket, (oldBucket, newBucketToAdd) -> {
            if (hasExpired(oldBucket, currentInstant)) return newBucketToAdd;
            else return bucketMerger.mergeBuckets(oldBucket, newBucketToAdd);
        });
        return true;
    }

    public DoubleSummaryStatistics getStats() {
        return storage.values().stream()
                .filter(bucket -> isTransactionWithinWindow(bucket, System.currentTimeMillis()))
                .map(Bucket::getDoubleSummaryStatistics)
                .reduce(new DoubleSummaryStatistics(), (dss1, dss2) -> {
                    dss1.combine(dss2);
                    return dss1;
                });
    }

    private boolean isTransactionWithinWindow(Bucket bucket, long currentTime){
        return !hasExpired(bucket, currentTime) && !isFromFuture(bucket, currentTime);
    }

    private boolean isFromFuture(Bucket bucket, long currentInstant) {
        return bucket.getTimestamp() > currentInstant;
    }

    private boolean hasExpired(long transactionTimestamp, long currentTimestamp) {
        return getSeconds(currentTimestamp - transactionTimestamp) > slidingWindowSize;
    }

    private boolean hasExpired(Bucket bucket, long currentTimestamp){
        return hasExpired(bucket.getTimestamp(), currentTimestamp);
    }

    private long getSeconds(long diff) {
        return diff / 1000;
    }

    private int getBucketIndex(long currentInstant, long transactionTimestamp){
        long diff = getSeconds(currentInstant - transactionTimestamp);
        return (int) (diff % slidingWindowSize);
    }

    private Bucket createNewBucket(Transaction transaction){
        DoubleSummaryStatistics newStats = new DoubleSummaryStatistics();
        newStats.accept(transaction.getAmount());
        return new Bucket(transaction.getTimestamp(), newStats);
    }
}
