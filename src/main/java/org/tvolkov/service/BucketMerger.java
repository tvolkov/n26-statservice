package org.tvolkov.service;

import org.springframework.stereotype.Component;

import java.util.DoubleSummaryStatistics;

import static java.util.Objects.requireNonNull;

@Component
class BucketMerger {
    Bucket mergeBuckets(Bucket oldBucket, Bucket newBucket){
        requireNonNull(oldBucket);
        requireNonNull(newBucket);
        DoubleSummaryStatistics doubleSummaryStatistics = new DoubleSummaryStatistics();
        doubleSummaryStatistics.combine(oldBucket.getDoubleSummaryStatistics());
        doubleSummaryStatistics.combine(newBucket.getDoubleSummaryStatistics());
        return new Bucket(oldBucket.getTimestamp(), doubleSummaryStatistics);
    }
}
