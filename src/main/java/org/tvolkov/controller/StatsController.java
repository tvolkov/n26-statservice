package org.tvolkov.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.tvolkov.service.StatsService;

import java.util.DoubleSummaryStatistics;

@Controller
public class StatsController {

    @Autowired
    private StatsService statsService;

    @GetMapping("/statistics")
    public ResponseEntity<DoubleSummaryStatistics> getStats(){
        return new ResponseEntity<>(statsService.getStats(), HttpStatus.OK);
    }
}
