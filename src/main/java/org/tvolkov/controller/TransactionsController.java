package org.tvolkov.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.tvolkov.dto.Transaction;
import org.tvolkov.service.StatsService;

@Controller
public class TransactionsController {

    @Autowired
    private StatsService statsService;

    @PostMapping(path = "/transactions")
    public ResponseEntity postTransaction(@RequestBody Transaction transaction){
        return new ResponseEntity(getResponseHeaders(transaction.getAmount()), getStatusCode(transaction));
    }

    private HttpHeaders getResponseHeaders(double amount) {
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("amount", String.valueOf(amount));
        responseHeaders.set("time", String.valueOf(System.currentTimeMillis()));
        return responseHeaders;
    }

    private HttpStatus getStatusCode(Transaction transaction) {
        return statsService.processTransaction(transaction) ? HttpStatus.CREATED : HttpStatus.NO_CONTENT;
    }
}
