package org.tvolkov.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.DoubleSummaryStatistics;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class BucketMergerTest {
    private BucketMerger bucketMerger;

    @Before
    public void setup(){
        bucketMerger = new BucketMerger();
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowExceptionIfFirstBucketIsNull(){
        //given
        Bucket b1 = null;
        Bucket b2 = new Bucket(-1, null);

        //when
        bucketMerger.mergeBuckets(b1, b2);
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowExceptionIfSecondBucketIsNull(){
        //given
        Bucket b1 = new Bucket(-1, null);
        Bucket b2 = null;

        //when
        bucketMerger.mergeBuckets(b1, b2);
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowExceptionIfBothBucketsAreNull(){
        //given
        Bucket b1 = null;
        Bucket b2 = null;

        //when
        bucketMerger.mergeBuckets(b1, b2);
    }

    @Test
    public void shouldMergeBuckets(){
        //given
        DoubleSummaryStatistics doubleSummaryStatistics = new DoubleSummaryStatistics();
        doubleSummaryStatistics.accept(1.0);
        Bucket b1 = new Bucket(System.currentTimeMillis(), doubleSummaryStatistics);

        DoubleSummaryStatistics doubleSummaryStatistics1 = new DoubleSummaryStatistics();
        doubleSummaryStatistics1.accept(2.0);
        Bucket b2 = new Bucket(System.currentTimeMillis(), doubleSummaryStatistics1);

        //when
        Bucket newBucket = bucketMerger.mergeBuckets(b1, b2);

        //then
        assertNotNull(newBucket);
        DoubleSummaryStatistics newStats = newBucket.getDoubleSummaryStatistics();
        assertEquals(3.0, newStats.getSum(), 0);
        assertEquals(1.5, newStats.getAverage(), 0);
        assertEquals(1.0, newStats.getMin(), 0);
        assertEquals(2.0, newStats.getMax(), 0);
        assertEquals(2, newStats.getCount());
    }
}