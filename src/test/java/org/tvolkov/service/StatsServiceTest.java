package org.tvolkov.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.tvolkov.dto.Transaction;

import java.util.DoubleSummaryStatistics;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class StatsServiceTest {
    @Mock
    private BucketMerger bucketMerger;

    @InjectMocks
    private StatsService statsService;

    @Before
    public void setup(){
        ReflectionTestUtils.setField(statsService, "slidingWindowSize", 60);
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowExceptionIfTransactionIsNull(){
        //given
        Transaction transaction = null;

        //when
        statsService.processTransaction(transaction);
    }

    @Test
    public void shouldReturnFalseIfTransactionIsOlderThanWindow(){
        //given
        Transaction transaction = new Transaction();
        transaction.setTimestamp(System.currentTimeMillis() - 70000);

        //when
        boolean result = statsService.processTransaction(transaction);

        //then
        assertFalse(result);
    }

    @Test
    public void shouldInsertNewBucketIfThereWereNoOlderBucket(){
        //given
        Transaction transaction = new Transaction();
        transaction.setTimestamp(System.currentTimeMillis());
        transaction.setAmount(1.0);

        //when
        boolean result = statsService.processTransaction(transaction);
        DoubleSummaryStatistics doubleSummaryStatistics = statsService.getStats();

        //then
        assertTrue(result);
        assertEquals(1, doubleSummaryStatistics.getCount());
    }

    @Test
    public void shouldInsertNewBucketIfOldBucketHasExpired() throws InterruptedException {
        //given
        long time = System.currentTimeMillis();
        Transaction transaction1 = new Transaction();
        transaction1.setTimestamp(time - 60500);
        transaction1.setAmount(1.0);
        Transaction transaction2 = new Transaction();
        transaction2.setTimestamp(time);
        transaction2.setAmount(2.0);

        //when
        boolean result1 = statsService.processTransaction(transaction1);
        Thread.sleep(500);
        boolean result2 = statsService.processTransaction(transaction2);
        DoubleSummaryStatistics doubleSummaryStatistics = statsService.getStats();

        //then
        assertTrue(result1);
        assertTrue(result2);
        assertEquals(1, doubleSummaryStatistics.getCount());
        assertEquals(2.0, doubleSummaryStatistics.getSum(), 0);
    }

    @Test
    public void shouldMergeTwoBuckets(){
        //given
        long time = System.currentTimeMillis();
        Transaction transaction1 = new Transaction();
        transaction1.setTimestamp(time);
        transaction1.setAmount(1.0);
        Transaction transaction2 = new Transaction();
        transaction2.setTimestamp(time);
        transaction2.setAmount(2.0);
        DoubleSummaryStatistics mergedStats = new DoubleSummaryStatistics();
        mergedStats.accept(transaction1.getAmount());
        mergedStats.accept(transaction2.getAmount());
        Bucket mergedBucket = new Bucket(time, mergedStats);
        when(bucketMerger.mergeBuckets(any(Bucket.class), any(Bucket.class))).thenReturn(mergedBucket);

        //when
        boolean result1 = statsService.processTransaction(transaction1);
        boolean result2 = statsService.processTransaction(transaction2);
        DoubleSummaryStatistics doubleSummaryStatistics = statsService.getStats();

        //then
        assertTrue(result1);
        assertTrue(result2);
        assertEquals(2, doubleSummaryStatistics.getCount());
        assertEquals(3.0, doubleSummaryStatistics.getSum(), 0);
    }

    @Test
    public void shouldNotGetStatsForTransactionsFromFuture(){
        //given
        Transaction transaction = new Transaction();
        transaction.setAmount(1.0);
        transaction.setTimestamp(System.currentTimeMillis() + 1000);

        //when
        boolean result = statsService.processTransaction(transaction);
        DoubleSummaryStatistics stats = statsService.getStats();

        //then
        assertTrue(result);
        assertEquals(0, stats.getCount());
    }

    @Test
    public void shouldReturnNothingIfAllBucketsAreExpired() throws InterruptedException {
        //given
        Transaction transaction1 = new Transaction();
        transaction1.setAmount(1.0);
        transaction1.setTimestamp(System.currentTimeMillis() + 60500);
        Transaction transaction2 = new Transaction();
        transaction2.setAmount(1.0);
        transaction2.setTimestamp(System.currentTimeMillis() + 60500);

        //when
        boolean result1 = statsService.processTransaction(transaction1);
        boolean result2 = statsService.processTransaction(transaction2);
        Thread.sleep(500);
        DoubleSummaryStatistics statistics = statsService.getStats();

        //then
        assertTrue(result1);
        assertTrue(result2);
        assertEquals(0, statistics.getCount());

    }
}