package org.tvolkov.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.tvolkov.service.StatsService;

import java.util.DoubleSummaryStatistics;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(StatsController.class)
public class StatsControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private StatsService statsService;

    @Test
    public void shouldReturnStats() throws Exception {
        DoubleSummaryStatistics doubleSummaryStatistics = new DoubleSummaryStatistics();
        doubleSummaryStatistics.accept(1.0);
        doubleSummaryStatistics.accept(2.0);
        given(statsService.getStats()).willReturn(doubleSummaryStatistics);

        mockMvc.perform(get("/statistics"))
                .andExpect(status().isOk())
                .andExpect(content().json("{count=2, sum=3.0, min=1.0, max=2.0, average=1.5}"));
    }
}