package org.tvolkov.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.tvolkov.dto.Transaction;
import org.tvolkov.service.StatsService;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(TransactionsController.class)
public class TransactionsControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private StatsService statsService;

    @Test
    public void shouldReturn201IfTransactionWithinWindow() throws Exception {
        given(statsService.processTransaction(any(Transaction.class))).willReturn(true);

        mockMvc.perform(post("/transactions")
        .contentType(MediaType.APPLICATION_JSON)
                .content("{\"amount\": 12.3,\"timestamp\": 1478192204000}"))
                .andExpect(status().isCreated())
                .andExpect(content().string(""))
        .andExpect(header().string("amount", "12.3"))
        .andExpect(header().exists("time"));
    }

    @Test
    public void shouldReturn204IfTransactionOutsideWindow() throws Exception {
        given(statsService.processTransaction(any(Transaction.class))).willReturn(false);

        mockMvc.perform(post("/transactions")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"amount\": 12.3,\"timestamp\": 1478192204000}"))
                .andExpect(status().isNoContent())
                .andExpect(content().string(""))
                .andExpect(header().string("amount", "12.3"))
                .andExpect(header().exists("time"));
    }
}