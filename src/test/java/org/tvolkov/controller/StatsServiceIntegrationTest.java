package org.tvolkov.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.tvolkov.App;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.hamcrest.CoreMatchers.is;

@RunWith(SpringRunner.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.MOCK,
        classes = App.class)
@AutoConfigureMockMvc
@TestPropertySource(
        locations = "classpath:application-integrationtest.properties")
public class StatsServiceIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void shouldReturnStatsForTransactionWithinWindow() throws Exception {
        mockMvc.perform(post("/transactions")
                .contentType(MediaType.APPLICATION_JSON)
                .content(createJsonPayload(1.0)))
                .andExpect(status().isCreated())
                .andExpect(content().string(""))
                .andExpect(header().string("amount", "1.0"))
                .andExpect(header().exists("time"));

        mockMvc.perform(get("/statistics"))
                .andExpect(status().isOk())
                .andExpect(content().json("{count=1, sum=1.0, min=1.0, max=1.0, average=1.0}"));

    }

    @Test
    public void shouldNotReturnStatsForTransactionsOutsideWindow() throws Exception {
        mockMvc.perform(post("/transactions")
                .contentType(MediaType.APPLICATION_JSON)
                .content(createJsonPayload(1.0, System.currentTimeMillis() - 61000)))
                .andExpect(status().isNoContent())
                .andExpect(content().string(""))
                .andExpect(header().string("amount", "1.0"))
                .andExpect(header().exists("time"));

        mockMvc.perform(get("/statistics"))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.count", is(0)))
                .andExpect(jsonPath("$.sum", is(0.0)))
                .andExpect(jsonPath("$.average", is(0.0)));

    }

    private String createJsonPayload(double amount){
        return createJsonPayload(amount, System.currentTimeMillis());
    }

    private String createJsonPayload(double amount, long timestamp){
        return "{\"amount\": " + amount + ",\"timestamp\": " + timestamp + "}";
    }
}
